# psyplot-DKRZ-TechTalk-20201117
Presentation materials for the TechTalk about psyplot at the [German Climate Computing Center (DKRZ)][DKRZ], November 17th, 2020

Content will be available soon. If you want to be notified, login to Github and select _Releases only_ from the _Watch_ dropdown in the upper right corner.


[DKRZ]: https://www.dkrz.de/
